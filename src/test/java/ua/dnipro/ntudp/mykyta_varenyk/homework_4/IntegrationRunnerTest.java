package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.config.SpringIntegrationConfiguration;

import java.io.File;

public class IntegrationRunnerTest {

    private static final String FILE_NAME = "src/test/resources/orders.csv";

    @Test
    public void mainTest(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(SpringIntegrationConfiguration.class);

        context.refresh();

        context.getBean(OrderIntegration.class).process(new File(FILE_NAME));

        OrderList orderList = context.getBean(OrderListImpl.class);

        context.close();
    }
}