package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import java.io.File;

@MessagingGateway
public interface OrderIntegration {
    @Gateway(requestChannel = "orderFlow.input")
    void process(File recievedFile);
}
