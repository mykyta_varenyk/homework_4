package ua.dnipro.ntudp.mykyta_varenyk.homework_4.config;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.SendMail;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.UserItemProcessor;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfiguration {
    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Bean
    public DataSource dataSource() {
        MysqlDataSource mysqlDataSource = null;
        try {
            mysqlDataSource = new MysqlConnectionPoolDataSource();
            mysqlDataSource.setURL("jdbc:mysql://localhost:3306/homework_4_schema");
            mysqlDataSource.setUser("root");
            mysqlDataSource.setPassword("15092023Mykyta");
            mysqlDataSource.setServerTimezone("UTC");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return mysqlDataSource;
    }

    @Bean
    public JdbcCursorItemReader<User> itemReader(){
        return new JdbcCursorItemReaderBuilder<User>()
                .dataSource(dataSource())
                .name("userReader")
                .sql("SELECT email,account_balance FROM users")
                .rowMapper(new UserMapper())
                .build();
    }


    @Bean
    public UserItemProcessor processor(){
        return new UserItemProcessor();
    }

    @Bean
    public SendMail sendMail(){
        return new SendMail();
    }

    @Bean
    public Job importUserJob(){
        return jobs.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1(){
        System.out.println("step1");
        return steps.get("step1")
                .<User,User>chunk(2)
                .reader(itemReader())
                .processor(processor())
                .writer(sendMail())
                .build();
    }

    private class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs,int rowNum) throws SQLException {
            User user = new User();
            user.setEmail(rs.getString("email"));
            user.setAccountBalance(rs.getInt("account_balance"));
            return user;
        }
    }
}
