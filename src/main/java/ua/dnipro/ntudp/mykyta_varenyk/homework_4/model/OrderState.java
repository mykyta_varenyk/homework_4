package ua.dnipro.ntudp.mykyta_varenyk.homework_4.model;

public enum OrderState {
    CANCELED,
    WAITING_FOR_PAYMENT,
    PAYMENT_COMPLETED;


}