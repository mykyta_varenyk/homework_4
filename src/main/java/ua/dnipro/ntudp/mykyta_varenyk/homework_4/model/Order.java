package ua.dnipro.ntudp.mykyta_varenyk.homework_4.model;

public class Order {
    private OrderState orderState;
    private int id;
    private int userId;

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isNotCanceled(){
        return orderState != OrderState.CANCELED;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderState=" + orderState +
                ", id=" + id +
                ", userId=" + userId +
                '}';
    }
}
