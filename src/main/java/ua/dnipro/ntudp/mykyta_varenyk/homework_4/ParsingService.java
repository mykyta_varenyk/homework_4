package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.Order;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.OrderState;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Service
public class ParsingService {
    @Transformer
    public static List<Order> retrieveOrdersFromCSV(File myFile){

        List<List<String>> records = new ArrayList<>();

        List<Order> orders = new ArrayList<>();

        try(Scanner scanner = new Scanner(myFile)) {
            while (scanner.hasNextLine()){
                records.add(getRecordFromLine(scanner.nextLine()));
            }

            for (List<String> list : records) {
                Order order = new Order();

                order.setId(Integer.parseInt(list.get(0)));

                order.setUserId(Integer.parseInt(list.get(1)));

                switch (list.get(2)){
                    case "CANCELED":
                        order.setOrderState(OrderState.CANCELED);
                        break;
                    case "WAITING_FOR_PAYMENT":
                        order.setOrderState(OrderState.WAITING_FOR_PAYMENT);
                        break;
                    case "PAYMENT_COMPLETED":
                        order.setOrderState(OrderState.PAYMENT_COMPLETED);
                        break;
                    default:
                        break;
                }
                orders.add(order);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return orders;
    }

    private static List<String> getRecordFromLine(String line){
        List<String> values = new ArrayList<>();

        try(Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");

            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }

        return values;
    }
}
