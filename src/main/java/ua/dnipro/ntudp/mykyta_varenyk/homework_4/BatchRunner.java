package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.config.SpringBatchConfiguration;

public class BatchRunner {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(SpringBatchConfiguration.class);

        context.refresh();

        JobLauncher jobLauncher = context.getBean(JobLauncher.class);

        Job job = context.getBean("importUserJob",Job.class);

        JobParameters jobParameters = new JobParametersBuilder().toJobParameters();

        System.out.println(job);

        System.out.println(jobParameters);

        try {
            JobExecution jobExecution = jobLauncher.run(job,jobParameters);
        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }
    }
}
