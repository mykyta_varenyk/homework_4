package ua.dnipro.ntudp.mykyta_varenyk.homework_4.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.OrderList;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.OrderListImpl;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.ParsingService;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.Order;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@IntegrationComponentScan("ua.dnipro.ntudp.mykyta_varenyk.homework_4")
@EnableIntegration
public class SpringIntegrationConfiguration {
    @Bean
    public DirectChannel outputChannel() {
        return new DirectChannel();
    }

    @Bean
    public OrderList orderList(){
        return new OrderListImpl();
    }

    @Bean
    public IntegrationFlow orderFlow() {
        return flow -> flow
              .transform(ParsingService::retrieveOrdersFromCSV)
              .channel("outputChannel");
    }

    @ServiceActivator(inputChannel = "outputChannel")
    public List<Order> filterOrders(List<Order> preFilteredOrders){
       OrderList orderList = orderList();

       List<Order> filteredOrders = preFilteredOrders.stream()
               .filter(order -> order.isNotCanceled())
               .collect(Collectors.toList());

        for (Order filteredOrder : filteredOrders) {
            orderList.addOrder(filteredOrder);
        }

        return filteredOrders;
    }
}