package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.Order;

public interface OrderList {
    void addOrder(Order order);
}
