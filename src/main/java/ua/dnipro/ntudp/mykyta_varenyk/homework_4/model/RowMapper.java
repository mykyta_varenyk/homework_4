package ua.dnipro.ntudp.mykyta_varenyk.homework_4.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
    T mapRow(ResultSet rs,int rowNum) throws SQLException;
}
