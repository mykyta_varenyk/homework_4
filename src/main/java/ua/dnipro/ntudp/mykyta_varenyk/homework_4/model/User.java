package ua.dnipro.ntudp.mykyta_varenyk.homework_4.model;

public class User {
    private String email;

    private int accountBalance;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }
}
