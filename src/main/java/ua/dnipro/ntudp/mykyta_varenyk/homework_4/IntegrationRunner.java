package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ua.dnipro.ntudp.mykyta_varenyk.homework_4.config.SpringIntegrationConfiguration;

import java.io.File;
public class IntegrationRunner {
    private static final String FILE_NAME = "src/main/resources/orders.csv";

    private static final Logger LOGGER = LogManager.getLogger(IntegrationRunner.class);

    public static void main(String[] args) {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

            context.register(SpringIntegrationConfiguration.class);

            context.refresh();

            context.getBean(OrderIntegration.class).process(new File(FILE_NAME));

            OrderList orderList = context.getBean(OrderListImpl.class);

            LOGGER.debug(orderList);

            System.out.println(orderList);

            context.close();
    }
}