package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.springframework.stereotype.Component;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.Order;

import java.util.ArrayList;

@Component
public class OrderListImpl extends ArrayList<Order> implements OrderList {
    @Override
    public void addOrder(Order order) {
        add(order);
    }

}
