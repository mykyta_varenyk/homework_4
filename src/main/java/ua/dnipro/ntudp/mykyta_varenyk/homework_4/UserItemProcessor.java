package ua.dnipro.ntudp.mykyta_varenyk.homework_4;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import ua.dnipro.ntudp.mykyta_varenyk.homework_4.model.User;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

import static javax.mail.internet.InternetAddress.parse;

@Component
public class UserItemProcessor implements ItemProcessor<User,User> {
    @Override
    public User process(User user) throws Exception {
        if(user.getAccountBalance()<10){
            Properties props = new Properties();
            props.setProperty("mail.smtp.host","smtp.gmail.com");
            props.setProperty("mail.smtp.port","465");
            props.setProperty("mail.smtp.auth","true");
            props.setProperty("mail.smtp.socketFactory.port","465");
            props.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");

            Session session = Session.getDefaultInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("mail@gmail","password");
                }
            });

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress("mykytavar@gmail.com"));

            message.setRecipients(Message.RecipientType.TO, parse(user.getEmail()));

            message.setSubject("balance lower than 10 $");

            String msg = "Your balance ist lower than 10 $";

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg,"text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            System.out.println(user.getEmail());

            Transport.send(message);

            System.out.println(message);

            return user;
        }
        return null;
    }
}